﻿/**
 *	FollowTransform.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	16/06/2019 (dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class FollowTransform : MonoBehaviour
{
    public Transform Target { get; private set; }

    public void SetTarget(Transform target)
    {
        Target = target;
    }

    private void Update()
    {
        transform.position = Target.position;
    }
}