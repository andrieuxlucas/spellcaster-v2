﻿/**
 *	CharacterAttributes.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class CharacterAttributes : MonoBehaviour
{
    public float HealthRegen { get { return _healthRegen; }  set { _healthRegen = value; } }
    public float MaxHealth { get { return _maxHealth; } set { _maxHealth = value; } }
    public float Health { get { return _health; } }

    public float ManaRegen { get { return _manaRegen; } set { _manaRegen = value; } }
    public float MaxMana { get { return _maxMana; } set { _maxMana = value; } }
    public float Mana { get { return _mana; } }

    [Header("Max values")]
    [SerializeField] private float _maxHealth;
    [SerializeField] private float _maxMana;

    [Header("Values")]
    [SerializeField] private float _health;
    [SerializeField] private float _mana;
    [SerializeField] private float _healthRegen;
    [SerializeField] private float _manaRegen;
    [SerializeField] private bool _healthRegenBlocked = false;
    [SerializeField] private bool _manaRegenBlocked = false;


    public void StopHealthRegen()
    {
        _healthRegenBlocked = true;
    }
    public void StopManaRegen()
    {
        _manaRegenBlocked = true;
    }

    public void ResumeHealthRegen()
    {
        _healthRegenBlocked = false;
    }
    public void ResumeManaRegen()
    {
        _manaRegenBlocked = false;
    }

    public void RestoreMana(float value)
    {
        _mana = Mathf.Clamp(_mana + value, 0, _maxMana);
    }
    public void DrainMana(float value)
    {
        _mana = Mathf.Clamp(_mana - value, 0, _maxMana);
    }
    public void Damage(float value)
    {
        _health = Mathf.Clamp(_health - value, 0, _maxHealth);
    }
    public void Heal(float value)
    {
        _health = Mathf.Clamp(_health + value, 0, _maxHealth);
    }

    private void Update()
    {
        if(!_healthRegenBlocked)
            if (_health < _maxHealth)
                _health = Mathf.Clamp(_health + (_healthRegen * Time.deltaTime), 0, _maxHealth);

        if(!_manaRegenBlocked)
            if (_manaRegen < _maxMana)
                _mana = Mathf.Clamp(_mana + (_manaRegen * Time.deltaTime), 0, _maxMana);
    }
}