﻿/**
 *	MovementRestrictionType.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	16/06/2019 (dd/mm/yy)
 */


public enum MovementRestrictionType
{
    None, Frontal_Only, Lateral_Ony, Total
}