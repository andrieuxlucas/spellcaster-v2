﻿/**
 *	ComboReaderComponent.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	27/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class ComboReaderComponent : MonoBehaviour
{
    public delegate void ComboEvent(string combo);
    public event ComboEvent ComboCompleted;

    [Header("Settings")]
    [SerializeField] private ComboReaderSettings _comboReaderSettings;
    [SerializeField] private string _currentString;

    private bool HasComboString { get { return !string.IsNullOrEmpty(_currentString); } }
    private KeySettings _keySettings;
    private Coroutine _autoConjureRoutine;


    private void Update()
    {
        ReadKeys();
    }

    public void SetKeySettings(KeySettings newKeySettings)
    {
        _keySettings = newKeySettings;

        enabled = true;
    }

    private void ReadKeys()
    {
        if(!_keySettings) { enabled = false; return; }

        foreach (KeyCode key in _keySettings.SkillKeys)
        {
            if(Input.GetKeyDown(key))
            {
                if (key != _keySettings.Key_Skill_Cast)
                {
                    _currentString += _keySettings.GetKeyRuneCode(key);

                    StartAutoConjureRoutine();

                    //Debug
                    print(_keySettings.GetKeyRuneName(key));
                }
                else
                {
                    if(HasComboString)
                        FinishCombo();
                }
            }
        }
    }

    private void StartAutoConjureRoutine()
    {
        StopAutoConjureRoutine();

        _autoConjureRoutine = StartCoroutine(AutoConjureRoutine());
    }

    private void StopAutoConjureRoutine()
    {
        if (_autoConjureRoutine != null)
        {
            StopCoroutine(_autoConjureRoutine);
            _autoConjureRoutine = null;
        }
    }

    private void FinishCombo()
    {
        if (ComboCompleted != null)
            ComboCompleted(_currentString);

        _currentString = string.Empty;

        StopAutoConjureRoutine();
    }


    private IEnumerator AutoConjureRoutine()
    {
        yield return new WaitForSecondsRealtime(_comboReaderSettings.MaxCaptureWaitTime);

        FinishCombo();
    }
}