﻿/**
 *	GameDefinitions.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	27/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public static class GameDefinitions
{
#if UNITY_EDITOR
    public const string SCRIPTABLE_SPELLS = "Spellcaster/Spells/";
#endif

    public static readonly LayerMask ExcludingPlayerLayerMask = new LayerMask()
    {
        value = LayerMask.GetMask("Default", "YuME_TileMap")
    };
    public static readonly string[] RuneNames = new string[] { "Thurs", "Quan", "Reios", "Yr", "Fen" };
    public static readonly char[] RuneCodes = new char[] { 'T', 'Q', 'R', 'Y', 'F' }; // { 1, 2, 3, Q, E}
    public static readonly byte MAX_SPELL_COUNT = 2;
    public static readonly string PLAYER_OBJECT_LAYER = "PlayerObject";

    private static Dictionary<KeyCode, string> _runeNameByKey;
    public static void SetRuneNamesByKey(KeyCode[] keys)
    {
        _runeNameByKey = new Dictionary<KeyCode, string>();

        for (int i = 1; i < keys.Length; i++)
            _runeNameByKey.Add(keys[i], RuneNames[i - 1]);
    }
    public static string GetRuneNameByKey(KeyCode key)
    {
        return _runeNameByKey[key];
    }
    public static char GetRuneCodeByKey(KeyCode key)
    {
        List<KeyCode> __keys = new List<KeyCode>(_runeNameByKey.Keys);

        if (__keys.Contains(key)) return RuneCodes[__keys.IndexOf(key)];
        else return ' ';
    }
}