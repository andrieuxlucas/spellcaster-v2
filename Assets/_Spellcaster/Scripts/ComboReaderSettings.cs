﻿/**
 *	ComboReaderSettings.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	27/01/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

/// <summary>
/// Class used to set and configure <see cref="ComboReaderComponent"/> properties.
/// </summary>
[CreateAssetMenu(fileName = "ComboReaderSettings", menuName = "Spellcaster/Settings/ComboReader")]
public class ComboReaderSettings : ScriptableObject
{
    public float MaxCaptureWaitTime { get { return _maxCaptureWaitTime; } set { _maxCaptureWaitTime = value; } }

    [SerializeField] private float _maxCaptureWaitTime = 0.5f;
}