﻿/**
 *	CustomSpell.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	10/06/2019 (dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Spells
{
    public interface ICustomSpell<T> where T : SpellBehaviour
    {
        T GetBehaviour();
        T Cast(Character caster, Vector3 castFrom, Quaternion castRotation);
    }

    public abstract class CustomSpell<BehaviourType> : Spell, ICustomSpell<BehaviourType> where BehaviourType : SpellBehaviour
    {
        [Header("Spell Behaviour")]
        [SerializeField] protected BehaviourType behaviour;

        BehaviourType ICustomSpell<BehaviourType>.Cast(Character caster, Vector3 castFrom, Quaternion castRotation)
        {
            return (BehaviourType)Cast(caster, castFrom, castRotation);
        }
        BehaviourType ICustomSpell<BehaviourType>.GetBehaviour()
        {
            return (BehaviourType)GetBehaviour();
        }

        protected override SpellBehaviour GetBehaviour()
        {
            return behaviour;
        }
    }
}