﻿using UnityEngine;

namespace Spells
{
    [RequireComponent(typeof(EffectsPossessor))]
    public abstract class SpellBehaviour : MonoBehaviour, IDebugableSpell
    {
        public static SpellBehaviour Cast(SpellBehaviour behaviour, Character caster, Vector3 castFrom, Quaternion castRotation)
        {
            SpellBehaviour instance = Instantiate(behaviour, castFrom, castRotation);
            instance.caster = caster;

            return instance;
        }
        public static T Cast<T>(T behaviour, Character caster, Vector3 castFrom, Quaternion castRotation) where T : SpellBehaviour
        {
            T instance = Instantiate(behaviour, castFrom, castRotation);
            instance.caster = caster;

            return instance;
        }

        public Character Caster { get { return caster; } }

        [Header("References")]
        [SerializeField] protected Character caster;
        [SerializeField] protected EffectsPossessor effecstContainer;

#if UNITY_EDITOR
        [Header("References")]
        [SerializeField] bool debug = true;

        private void LateUpdate()
        {
            if(debug) DebugSpell();
        }
#endif

        private void Awake()
        {
            if (!effecstContainer) effecstContainer = GetComponent<EffectsPossessor>();
        }

        private void Reset()
        {
            if (Application.isEditor)
                Awake();
        }

        public virtual void DebugSpell() { }


        public Effect[] GetCasterEffects()
        {
            return effecstContainer.casterEffects;
        }

        public Effect[] GetTargetEffects()
        {
            return effecstContainer.targetEffects;
        }


        public void SetTargetEfffects(Effect[] effects)
        {
            effecstContainer.targetEffects = effects;
        }

        public void SetCasterEffects(Effect[] effects)
        {
            effecstContainer.casterEffects = effects;
        }


        public void ApplyTargetEfects(EffectReceiver receiver)
        {
            foreach (var effect in GetTargetEffects())
                receiver.ApplyEffect(effect);
        }
    }

    public abstract class GenericSpellBehaviour<VisualType> : SpellBehaviour where VisualType : Visuals.SpellVisuals
    {
        [SerializeField] protected VisualType spellVisuals;

        private void Awake()
        {
            if (!spellVisuals) spellVisuals = GetComponent<VisualType>();
        }
    }
}