﻿/**
 *	CustomDirectionalSpell.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	02/07/2019 (dd/mm/yy)
 */

using ScanningMethods;
using UnityEngine;

namespace Spells
{
    public abstract class CustomDirectionalSpell<T> : GenericSpellBehaviour<T> where T : Spells.Visuals.SpellVisuals
    {
        public DirectionalScan DirectionalScan => directionalScan;

        [Header("Hit Registration")]
        [Tooltip("The physics collision layer this spell can target.")]
        public LayerMask scanableLayers;

        [Tooltip("The radius of the capsule which is used to cast the collision.")]
        public float collisionRadius = 0.3f;

        [Tooltip("The maximum distance from the casting point the spell will perform a collision detection.")]
        public float maxDistance = 20f;

        [Tooltip("The incremental step of the capsule casted each frame.")]
        public float speed = 10.0f;

        [Tooltip("The precision with which capsule cast is made. (Bigger numbers equals more precision)")]
        public int scanSubdivision = 2;

        [Tooltip("Wheter or not the scanning process should stop when it detects a collision")]
        public bool disableOnHit = true;

        DirectionalScan directionalScan;

        protected void SetupDirectionalScan(Vector3 InitialPosition, Vector3 InitialDirection, bool debug = true)
        {
            directionalScan = new DirectionalScan(new DirectionalConfig()
            {
                InitialPosition = InitialPosition,
                InitialDirection = InitialDirection,
                ScanableLayers = scanableLayers,
                Precision = scanSubdivision,

                DisableOnHit = disableOnHit,
                Debug = debug,

                ScanRadius = collisionRadius,
                MaxDistance = maxDistance,
                Speed = speed
            });
        }
        protected void SetupDirectionalScan(DirectionalConfig config)
        {
            directionalScan = new DirectionalScan(config);
        }
    }
}