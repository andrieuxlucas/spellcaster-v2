﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Spells.Visuals
{
    public abstract class SpellVisuals : MonoBehaviour
    {
        public abstract void EnableVisuals();
        public abstract void DiableVisuals();
        public abstract void CreateVisual();
    }
}