﻿/**
 *	Spells.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	27/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Spells
{
    public delegate void SpellCastAction(Spell spellCasted, Character caster);

    public interface ISpell
    {
        uint ManaCost { get; }
        uint Cooldown { get; }
        string Code { get; }

        SpellBehaviour GetBehaviour();
        SpellBehaviour Cast(Character caster, Vector3 castFrom, Quaternion castRotation);
    }

    /// <summary>
    /// Base class for all spells a <see cref="Character"/> can cast.
    /// </summary>
    public abstract class Spell : ScriptableObject, ISpell
    {
        public static event SpellCastAction OnSpellCasted;

        /// <summary>
        /// The base mana cost of this <see cref="Spell"/>.
        /// </summary>
        public uint ManaCost { get { return _baseManaCost; } }

        /// <summary>
        /// The base cooldown interval of this <see cref="Spell"/>.
        /// </summary>
        public uint Cooldown { get { return _baseCooldown; } }

        /// <summary>
        /// The 'Code', or key reference necessary to cast this <see cref="Spell"/>.
        /// </summary>
        public string Code { get { return _code; } }

        [Header("Code")]
        [SerializeField] protected string _code;

        [Header("Texts")]
        [SerializeField] protected string _name;
        [SerializeField] protected string _description;

        [Header("General values")]
        [SerializeField] protected uint _baseCooldown;
        [SerializeField] protected uint _baseManaCost;

        /// <summary>
        /// The array of <see cref="Effect"/> objects applied to the hitted target.
        /// </summary>
        [Header("Effects")]
        [SerializeField] protected Effect[] _targetEffects;

        /// <summary>
        /// The array of <see cref="Effect"/> objects applied to the caster <see cref="Character"/>.
        /// </summary>
        [SerializeField] protected Effect[] _casterEffects;

        SpellBehaviour ISpell.Cast(Character caster, Vector3 castFrom, Quaternion castRotation)
        {
            return Cast(caster, castFrom, castRotation);
        }
        SpellBehaviour ISpell.GetBehaviour()
        {
            return GetBehaviour();
        }

        protected virtual SpellBehaviour Cast(Character caster, Vector3 castFrom, Quaternion castRotation)
        {
            var instance = SpellBehaviour.Cast(GetBehaviour(), caster, castFrom, castRotation);
            instance.SetTargetEfffects(_targetEffects);
            instance.SetCasterEffects(_casterEffects);

            foreach (var casterEffect in instance.GetCasterEffects())
                if (casterEffect is IAppliedOnCast)
                    caster.ApplyEffect(casterEffect);

            return instance;
        }
        protected abstract SpellBehaviour GetBehaviour();
    }
}