﻿/**
 *	ICameraVisualEffectUser.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	20/06/2019 (dd/mm/yy)
 */

using UnityEngine.PostProcessing;
using UnityEngine;

public interface ICameraVisualEffectUser
{
    PostProcessingProfile GetPostProcessClass();
}