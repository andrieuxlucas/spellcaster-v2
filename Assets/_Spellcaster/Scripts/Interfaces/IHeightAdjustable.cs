﻿/**
 *	IHeightAdjustable.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	16/06/2019 (dd/mm/yy)
 */

public interface IHeightAdjustable
{
    void SetHeight(float newHeight);
}