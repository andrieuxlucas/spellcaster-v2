﻿/**
 *	ISpeedModulatable.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	(dd/mm/yy)
 */

public interface ISpeedModulatable
{
    void SetSpeedPercentage(float percentage);
}