﻿/**
 *	IntermitentReload.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace ParticleOperations
{
    public class IntermitentReload : ParticleOperator
    {
        [SerializeField] private float _waitTime = 5.0f;
        [SerializeField] private float _interval = 3.0f;

        private void Start()
        {
            InvokeRepeating("Reload", _waitTime, _interval);
        }

        private void Reload()
        {
            gameObject.SetActive(true);
        }
    }
}