﻿/**
 *	ParticleColorChanger.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace ParticleOperations
{
    [ExecuteInEditMode]
    public class ParticleColorChanger : ParticleOperator
    {   
        [Header("Colors")]
        [SerializeField] ParticleSystem.MinMaxGradient _startingColor;

        private void OnValidate()
        {
            if(_particleSystemArray != null)
            {
                foreach (var __particleSistem in _particleSystemArray)
                {
                    var __mainModule = __particleSistem.main;
                    __mainModule.startColor = _startingColor;
                }
            }
        }
    }
}