﻿/**
 *	CharacterInfoCanvas.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using TMPro;

public class CharacterInfoCanvas : MonoBehaviour
{
    [SerializeField] private Character _target;

    private CharacterAttributes _attributes;
    private TextMeshProUGUI _healthText;
    private TextMeshProUGUI _manaText;

    private void Awake()
    {
        if (_target)
            _attributes = _target.GetComponent<CharacterAttributes>();

        var _healthChildText = transform.Find("UpperPanel");
        if(_healthChildText)
            _healthText = _healthChildText.GetComponentInChildren<TextMeshProUGUI>();

        var _manaChildText = transform.Find("LowerPanel");
        if (_manaChildText)
            _manaText = _manaChildText.GetComponentInChildren<TextMeshProUGUI>();
    }

    private void Update()
    {
        _healthText.text = string.Format("{0}/{1}", Mathf.FloorToInt(_attributes.Health), _attributes.MaxHealth);
        _manaText.text = string.Format("{0:0.}/{1}", Mathf.FloorToInt(_attributes.Mana), _attributes.MaxMana);
    }
}