﻿/**
 *	CollisionHelper.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:		(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class CollisionHelper : MonoBehaviour
{
    public delegate void CollisionEvent(GameObject collisionObject, Vector3 collisionPosition);
    public event CollisionEvent OnCollision;


    private List<ParticleCollisionEvent> _collisionEvents;
    private ParticleSystem _particleSystem;
    private Vector3 _collisionPosition;

    [Header("Values")]
    [SerializeField] private float _collisionRadius = 5.0f;

    [Header("Options")]
    [SerializeField] private bool _debug = false;


    private void OnParticleCollision(GameObject other)
    {
        int __collisionCount = _particleSystem.GetCollisionEvents(other, _collisionEvents);
        if (_collisionEvents.Count > 0)
        {
            if (_debug)
                _collisionPosition = _collisionEvents[0].intersection;

            Debug.LogWarningFormat("Hitted: {0}", other.name);

            if (OnCollision != null)
                OnCollision(other, _collisionEvents[0].intersection);
        }
    }

    private void OnDrawGizmos()
    {
        if(_debug)
        {
            if (_collisionPosition == null) return;

            Color color = Color.red;

            color.a = .2f;
            Gizmos.color = color;
            Gizmos.DrawSphere(_collisionPosition, _collisionRadius);

            color.a = 1.0f;
            Gizmos.color = color;
            Gizmos.DrawWireSphere(_collisionPosition, _collisionRadius);
        }
    }

    private void Start()
    {
        _collisionEvents = new List<ParticleCollisionEvent>();
        _particleSystem = GetComponent<ParticleSystem>();
    }
}