﻿/**
 *	ParticleOperator.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace ParticleOperations
{
    [ExecuteInEditMode]
    public abstract class ParticleOperator : MonoBehaviour
    {
        [Header("Components")]
        protected ParticleSystem[] _particleSystemArray;

        private void Awake()
        {
            _particleSystemArray = GetComponentsInChildren<ParticleSystem>(true);
        }
    }
}