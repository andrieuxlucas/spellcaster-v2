﻿/**
 *	KeySettings.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	27/11/2018	(dd/mm/yy)
 */

using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "KeySettings", menuName = "Spellcaster/Keybinds/KeySettings")]
public class KeySettings : ScriptableObject
{
    public KeyCode[] SkillKeys { get { return _skillKeys.ToArray(); } }
    public KeyCode Key_Skill_Cast { get { return _skillKeys[0]; } }
    public KeyCode Key_Skill_1 { get { return _skillKeys[1]; } }
    public KeyCode Key_Skill_2 { get { return _skillKeys[2]; } }
    public KeyCode Key_Skill_3 { get { return _skillKeys[3]; } }
    public KeyCode Key_Skill_4 { get { return _skillKeys[4]; } }
    public KeyCode Key_Skill_5 { get { return _skillKeys[5]; } }

    [Header("Skill keys list")]
    [SerializeField] private List<KeyCode> _skillKeys;

    private void Awake()
    {
        _skillKeys = new List<KeyCode>() { KeyCode.R, KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Q, KeyCode.E  };
    }

    public string GetKeyRuneName(KeyCode key)
    {
        return GameDefinitions.GetRuneNameByKey(key);
    }
    public char GetKeyRuneCode(KeyCode key)
    {
        return GameDefinitions.GetRuneCodeByKey(key);
    }
}