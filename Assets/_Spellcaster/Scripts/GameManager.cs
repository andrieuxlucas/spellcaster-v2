﻿/**
 *	GameManager.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	27/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private KeySettings _keySettings;

    private static GameManager CreateInstance()
    {
        GameObject __managerObject = new GameObject("GameManager");
        return __managerObject.AddComponent<GameManager>();
    }
    private static GameManager _instance;
    public static GameManager Instance
    {
        get { if (!_instance) _instance = CreateInstance(); return _instance; }
        set { if (_instance) Destroy(_instance.gameObject); _instance = value; }
    }

    private void OnDisable()
    {
        Destroy(gameObject);
    }
    private void OnEnable()
    {
        Instance = this;

        OnSessionStart();
    }
    private void Start()
    {
        OnMatchStart();
    }

    private void OnSessionStart()
    {
        if (_keySettings)
            GameDefinitions.SetRuneNamesByKey(_keySettings.SkillKeys);
    }
    private void OnMatchStart()
    {
        ComboReaderComponent __comboReader = FindObjectOfType<PlayerController>().Character.CombroReaderComponent;
        __comboReader.SetKeySettings(_keySettings);
    }
}