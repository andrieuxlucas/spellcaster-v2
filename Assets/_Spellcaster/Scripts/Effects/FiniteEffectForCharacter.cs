﻿/**
 *	FiniteEffectForCharacter.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	30/11/2018	(dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FiniteEffectForCharacter : FiniteEffectFor<Character>
{
	
}