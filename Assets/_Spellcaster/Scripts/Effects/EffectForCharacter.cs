﻿/**
 *	EffectForCharacter.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public abstract class EffectForCharacter : EffectFor<Character>
{
	
}