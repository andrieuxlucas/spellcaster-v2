﻿/**
 *	CharacterEffectReceiver.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine;
using System;

public class CharacterEffectReceiver : EffectReceiverFor<Character>
{
    public override Effect[] GetEffects()
    {
        return new Effect[0];
    }


    public override void ApplyEffect(EffectFor<Character> effect)
    {
        effect.ApplyEffect(Target);

        Debug.LogWarningFormat("@{0} - Effect: ({1}) applied to: ({2})",
                GetType().Name, effect.Name, Target.name);
    }

    public override void ApplyEffect(FiniteEffectFor<Character> effect)
    {
        effect.ApplyEffect(Target);

        if (!effect.IsPermanent)
            RemoveEffectDelayed(effect);

        Debug.LogWarningFormat("@{0} - Effect: ({1}) applied to: ({2})",
        GetType().Name, effect.Name, Target.name);
    }


    public override bool HasEffect<T>()
    {
        foreach (var effect in GetEffects())
            if (effect is T)
                return true;

        return false;
    }

    public override bool HasEffect(Type effectType)
    {
        foreach (var effect in GetEffects())
            if (effectType.Equals(effect.GetType()))
                return true;

        return false;
    }
}