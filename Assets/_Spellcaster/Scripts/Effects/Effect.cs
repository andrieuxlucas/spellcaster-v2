﻿/**
 *	Effect.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public abstract class Effect : ScriptableObject
{
    public string Description { get { return _description; } }
    public string Name { get { return _name; } }

    [Header("Texts")]
    [SerializeField] protected string _name;
    [SerializeField] protected string _description;

    protected abstract void ApplyEffect(object target);
}

public abstract class FiniteEffect : Effect
{
    public float Duration { get { return _duration; } }
    public bool IsPermanent { get { return _permanent; } }

    [Header("Timed values")]
    [SerializeField] protected float _duration;
    [SerializeField] protected bool _permanent;

    protected abstract void RemoveEffect(object target);
}

public abstract class FiniteEffectFor<T> : FiniteEffect where T : MonoBehaviour
{
    protected override void RemoveEffect(object target)
    {
        if (target is T)
            RemoveEffect(target as T);
    }

    public abstract void RemoveEffect(T target);


    protected override void ApplyEffect(object target)
    {
        if (target is T)
            ApplyEffect(target as T);
    }

    public abstract void ApplyEffect(T target);
}

public abstract class EffectFor<T> : Effect where T : MonoBehaviour
{
    protected override void ApplyEffect(object target)
    {
        if (target is T)
            ApplyEffect(target as T);
    }

    public abstract void ApplyEffect(T target);
}