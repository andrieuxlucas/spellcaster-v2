﻿/**
 *	EffectReceiver.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public abstract class EffectReceiver : MonoBehaviour
{
    public abstract Effect[] GetEffects();

    public abstract void ApplyEffect(Effect effect);

    public abstract bool HasEffect<T>() where T : Effect;
    public abstract bool HasEffect(System.Type effectType);

    protected abstract void RemoveEffectDelayed(FiniteEffect effect, object target);
}

public abstract class EffectReceiverFor<T> : EffectReceiver where T : MonoBehaviour
{
    public T Target { get { return _target; } set { _target = value; } }

    [Header("Effect target")]
    [SerializeField] T _target;

    protected override void RemoveEffectDelayed(FiniteEffect effect, object target)
    {
        if (effect is FiniteEffectFor<T> && target is T)
            RemoveEffectDelayed(effect as FiniteEffectFor<T>);
    }

    protected Coroutine RemoveEffectDelayed(FiniteEffectFor<T> effect)
    {
        return StartCoroutine(RemoveEffectCoroutine(effect));
    }


    public abstract void ApplyEffect(FiniteEffectFor<T> effect);

    public abstract void ApplyEffect(EffectFor<T> effect);

    public override void ApplyEffect(Effect effect)
    {
        if (effect is EffectFor<T>)
            ApplyEffect(effect as EffectFor<T>);

        else if (effect is FiniteEffectFor<T>)
            ApplyEffect(effect as FiniteEffectFor<T>);
    }


    protected virtual IEnumerator RemoveEffectCoroutine(FiniteEffectFor<T> effect)
    {
        yield return new WaitForSeconds(effect.Duration);

        effect.RemoveEffect(Target);
    }
}