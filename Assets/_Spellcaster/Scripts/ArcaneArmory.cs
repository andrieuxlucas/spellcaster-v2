﻿/**
 *	ArcaneArmory.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:		(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

using Spells;

/// <summary>
/// Main component used to store owned <see cref="Spell"/> and cast them.
/// </summary>
public class ArcaneArmory : MonoBehaviour
{
    public Spell[] SpellList { get { return _spellList.ToArray(); } }

    [Header("Containers")]
    [SerializeField] List<Spell> _spellList;

    private Queue<Spell> _spellHand;

    private void Awake()
    {
        if (_spellList == null) _spellList = new List<Spell>();

        _spellHand = new Queue<Spell>();
    }

    /// <summary>
    /// Casts the previously conjured <see cref="Spell"/>.
    /// </summary>
    /// <param name="caster">The <see cref="Character"/> who casted the spell.</param>
    /// <param name="castFrom">The point from where the spell is being casted.</param>
    /// <param name="castRotation">The world rotation of the object casting the spell.</param>
    public void CastSecondarySpell(Character caster, Vector3 castFrom, Quaternion castRotation)
    {
        ISpell __secondarySpell;

        // If we have a secondary spell conjured in our hand
        if (IsSecondarySpellConjured(_spellHand.ToArray(), out __secondarySpell))
        {
            // And the casters mana is higher then the mana cost
            if (caster.Attributes.Mana >= __secondarySpell.ManaCost)
            {
                caster.Attributes.DrainMana(__secondarySpell.ManaCost);

                __secondarySpell.Cast(caster, castFrom, castRotation);
            }
        }
    }

    /// <summary>
    /// Cast the more recently conjured <see cref="Spell"/>.
    /// </summary>
    /// <param name="caster">The <see cref="Character"/> who casted the spell.</param>
    /// <param name="castFrom">The point from where the spell is being casted.</param>
    /// <param name="castRotation">The world rotation of the object casting the spell.</param>
    public void CastPrimarySpell(Character caster, Vector3 castFrom, Quaternion castRotation)
    {
        ISpell __primarySpell;

        // If we have a primary spell conjured in our hand
        if (IsPrimarySpellConjured(_spellHand.ToArray(), out __primarySpell))
        {
            // And the casters mana is higher then the mana cost
            if (caster.Attributes.Mana >= __primarySpell.ManaCost)
            {
                caster.Attributes.DrainMana(__primarySpell.ManaCost);

                __primarySpell.Cast(caster, castFrom, castRotation);
            }
        }
    }


    /// <summary>
    /// Conjures a <see cref="Spell"/> contained inside the <see cref="OwnedSpells"/>.
    /// </summary>
    /// <param name="spellCode">The <see cref="Spell.Code"/> of the target <see cref="Spell"/> you whish to cast.</param>
    public void Conjure(string spellCode)
    {
        foreach (Spell __spell in _spellList)
        {
            // Checks if the conjured spellCode is contained into one of our spells.
            if (__spell.Code.Equals(spellCode))
            {
                _spellHand.Enqueue(__spell);
                print("Added spell " + __spell + " to hand");

                // If we already reached the conjured spells limit, we release the firts conjured spell
                if (_spellHand.Count > GameDefinitions.MAX_SPELL_COUNT)
                    _spellHand.Dequeue();

                return;
            }
        }

        Debug.LogError("Character does not posses any spell with those runes!");
    }

    bool IsSecondarySpellConjured(Spell[] spellHand, out ISpell secondarySpell)
    {
        secondarySpell = null;

        if (spellHand.Length == 2)
        {
            secondarySpell = spellHand[0];
            return true;
        }

        return false;
    }

    bool IsPrimarySpellConjured(Spell[] spellHand, out ISpell primarySpell)
    {
        primarySpell = null;

        if(spellHand.Length > 0)
        {
            primarySpell = spellHand.Length == 1 ? spellHand[0] : spellHand[1];
            return true;
        }

        return false;
    }
}