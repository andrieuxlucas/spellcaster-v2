﻿/**
 *	CharacterAttributesInspector.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CharacterAttributes))]
public class CharacterAttributesInspector : Editor
{
    SerializedProperty _characterHealthRegen;
    SerializedProperty _characterManaRegen;

    SerializedProperty _characterMaxHealth;
    SerializedProperty _characterHealth;

    SerializedProperty _characterMaxMana;
    SerializedProperty _characterMana;

    CharacterAttributes _character;

    private void OnEnable()
    {
        _character = (CharacterAttributes)target;

        _characterHealthRegen = serializedObject.FindProperty("_healthRegen");
        _characterManaRegen = serializedObject.FindProperty("_manaRegen");

        _characterMaxHealth = serializedObject.FindProperty("_maxHealth");
        _characterHealth = serializedObject.FindProperty("_health");

        _characterMaxMana = serializedObject.FindProperty("_maxMana");
        _characterMana = serializedObject.FindProperty("_mana");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        ValueBar(_characterHealth.floatValue / _characterMaxHealth.floatValue, "Health");
        ValueBar(_characterMana.floatValue / _characterMaxMana.floatValue, "Mana");

        GUILayout.BeginVertical();
        {
            GUILayout.BeginHorizontal();
            {
                EditorGUILayout.Slider(_characterHealth, 0, _characterMaxHealth.floatValue, new GUIContent("Health"));
                _characterMaxHealth.floatValue = FieldValue(_characterMaxHealth.floatValue);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            {
                EditorGUILayout.Slider(_characterMana, 0, _characterMaxMana.floatValue, new GUIContent("Mana"));
                _characterMaxMana.floatValue = FieldValue(_characterMaxMana.floatValue);
            }
            GUILayout.EndHorizontal();
        }
        GUILayout.EndVertical();

        GUILayout.Space(10);

        _characterHealthRegen.floatValue = CampValue(_characterHealthRegen.floatValue, "Health regen");
        _characterManaRegen.floatValue = CampValue(_characterManaRegen.floatValue, "Mana regen");

        serializedObject.ApplyModifiedProperties();
    }

    private float CampValue(float value, string label)
    {
        Rect __rect = GUILayoutUtility.GetRect(18, 18, "TextField");

        var __value = EditorGUI.FloatField(__rect, label, value);

        return __value;
    }

    private float FieldValue(float value)
    {
        Rect __rect = GUILayoutUtility.GetRect(18, 18, "TextField");

        var __value = EditorGUI.FloatField(__rect, value);
        EditorGUILayout.Space();

        return __value;
    }

    private void ValueBar(float value, string label)
    {
        Rect __rect = GUILayoutUtility.GetRect(18, 18, "TextField");

        EditorGUI.ProgressBar(__rect, value, label);
        EditorGUILayout.Space();
    }
}