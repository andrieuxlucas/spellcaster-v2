﻿/**
 *	MovementImpairmentEffect.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	16/06/2019 (dd/mm/yy)
 */

using UnityEngine;

namespace Effects
{
    [CreateAssetMenu(menuName = "Spellcaster/Effects/Movement impairment")]
    public class MovementImpairmentEffect : FiniteEffectForCharacter, IAppliedOnCast, IAppliedOnHit
    {
        public override void ApplyEffect(Character target)
        {
            target.SetSpeedPercentage(0f);
        }

        public override void RemoveEffect(Character target)
        {
            target.SetSpeedPercentage(1f);
        }
    }
}