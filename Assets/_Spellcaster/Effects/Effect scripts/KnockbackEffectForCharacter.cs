﻿/**
 *	KnockbackEffectForCharacter.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	14/06/2019 (dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Effects
{
    [CreateAssetMenu(menuName = "Spellcaster/Effects/Knockback")]
    public class KnockbackEffectForCharacter : EffectFor<Character>
    {
        public Vector3 KnockbackForce;

        public override void ApplyEffect(Character target)
        {
            target.AddForce(KnockbackForce);
        }
    }
}