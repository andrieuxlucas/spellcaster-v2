﻿/**
 *	Curse.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	30/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Spellcaster/Effects/Curse")]
public class Curse : FiniteEffectForCharacter
{
    public override void ApplyEffect(Character target)
    {
        target.Attributes.StopHealthRegen();
        target.Attributes.StopManaRegen();
    }

    public override void RemoveEffect(Character target)
    {
        target.Attributes.ResumeHealthRegen();
        target.Attributes.ResumeManaRegen();
    }
}