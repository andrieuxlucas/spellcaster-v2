﻿/**
 *	DamageEffect.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Effects
{
    [CreateAssetMenu(menuName = "Spellcaster/Effects/Damage")]
    public class DamageEffect : EffectForCharacter
    {
        public float DamageAmount = 10f;

        public override void ApplyEffect(Character target)
        {
            target.Attributes.Damage(DamageAmount);
        }
    }
}