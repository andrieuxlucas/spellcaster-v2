﻿using UnityEngine;

public class EffectsPossessor : MonoBehaviour
{
    public bool HasEffects => casterEffects.Length > 0 || targetEffects.Length > 0;
    public bool HasCasterEffects => casterEffects.Length > 0;

    public Effect[] casterEffects;
    public Effect[] targetEffects;

    public T GetTargetEffect<T>() where T : Effect
    {
        foreach (var effect in targetEffects)
            if (effect is T)
                return (T)effect;

        return null;
    }
    public T GetCasterEffect<T>() where T : Effect
    {
        foreach (var effect in casterEffects)
            if (effect is T)
                return (T)effect;

        return null;
    }
}