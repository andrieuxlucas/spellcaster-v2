﻿/**
 *	ShadowCrwalVisualsTask.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	20/06/2019 (dd/mm/yy)
 */

public class ShadowCrwalVisualsTask : TaskFor<ShadowCrawlCameraVisuals>
{
    public ShadowCrwalVisualsTask(ShadowCrawlCameraVisuals behaviour) : base(behaviour) { }

    public override float GetDuration()
    {
        return SpecializedComponent.Duration;
    }

    protected override void Update(float delta)
    {
        SpecializedComponent.SetEffects(delta);
    }

    protected override void OnPreUpdate()
    {
        SpecializedComponent.EnableColorGrading();
    }

    protected override void OnPostUpdate()
    {
        SpecializedComponent.DisableColorGrading();
    }
}