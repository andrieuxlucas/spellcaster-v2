﻿/**
 *	ShadowCrawlBehaviour.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	15/06/2019 (dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Spells.Behaviours
{
    public class ShadowCrawlBehaviour : GenericSpellBehaviour<Visuals.ShadowCrawlVisuals>
    {
        public float Delta => Mathf.Min((Time.time - startingTime) / duration, 1);
        [SerializeField] ShadowCrawlCameraVisuals cameraVisuals;

        [Header("Settings")]
        [SerializeField] float duration;
        [SerializeField] float force = 1f;
        [SerializeField] float acceleration = 35f;
        [SerializeField] AnimationCurve dipCurve;

        Vector3 casterForward;
        float startingTime;

        public void SetDuration(float duration)
        {
            this.duration = duration;
        }

        public override void DebugSpell()
        {

        }


        void Start()
        {
            startingTime = Time.time;
            casterForward = caster.transform.forward.normalized;
            caster.MovementComponent.AddForce(casterForward * force);

            spellVisuals.CreateVisual();
            spellVisuals.SetTarget(caster.transform);

            cameraVisuals.SetupVisuals(caster.Perspective, duration);
        }

        void Update()
        {
            (caster as IHeightAdjustable).SetHeight(dipCurve.Evaluate(Delta));
            caster.AddRelativeForce(Vector3.forward * acceleration * Time.deltaTime);
            //caster.MovementComponent.AddRelativeForce(new Vector3(0, 0, -acceleration * Time.deltaTime));

            if (Delta >= 1.0f)
            {
                spellVisuals.DiableVisuals();
                Destroy(gameObject);
            }
        }
    }
}