﻿/**
 *	ShadowCrawlVisuals.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	15/06/2019 (dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Spells.Visuals
{
    public class ShadowCrawlVisuals : SpellVisuals
    {
        public Transform Target { get; private set; }
        public GameObject SphereParticles;
        public GameObject ShadowExplosions;

        GameObject sphereInstance;
        GameObject explosionInstance;

        public void SetTarget(Transform target)
        {
            Target = target;
            explosionInstance = Instantiate(ShadowExplosions, Target.position, Quaternion.identity);
        }

        public override void CreateVisual()
        {
            sphereInstance = Instantiate(SphereParticles, transform.position, transform.rotation);
        }

        public override void DiableVisuals()
        {
            Destroy(sphereInstance, 2.0f);
            Destroy(explosionInstance, 1.0f);
        }

        public override void EnableVisuals() { }


        private void Update()
        {
            sphereInstance.transform.position = Target.position;
            sphereInstance.transform.rotation = Target.rotation;
        }
    }
}