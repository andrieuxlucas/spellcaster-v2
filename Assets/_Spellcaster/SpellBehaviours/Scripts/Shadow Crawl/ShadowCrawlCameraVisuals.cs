﻿/**
 *	ShadowCrawlCameraVisuals.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	20/06/2019 (dd/mm/yy)
 */

using UnityEngine.PostProcessing;
using UnityEngine;

public class ShadowCrawlCameraVisuals : MonoBehaviour
{
    public PostProcessingProfile Profile { get; private set; }
    public float Duration { get; private set; }

    public AnimationCurve exposureCurve;
    public AnimationCurve temperatureCurve;
    public AnimationCurve saturationCurve;
    public AnimationCurve constrastCurve;

    public AnimationCurve red_R_Curve;

    public AnimationCurve green_R_Curve;
    public AnimationCurve green_G_Curve;
    public AnimationCurve green_B_Curve;

    public AnimationCurve blue_R_Curve;
    public AnimationCurve blue_G_Curve;
    public AnimationCurve blue_B_Curve;

    private ShadowCrwalVisualsTask visualsTask;

    public void EnableColorGrading() { Profile.colorGrading.enabled = true; }
    public void DisableColorGrading() { Profile.colorGrading.enabled = false; }

    public void SetEffects(float delta)
    {
        ColorGradingModel.Settings modelSettings = Profile.colorGrading.settings;

        modelSettings.basic.postExposure = exposureCurve.Evaluate(delta);
        modelSettings.basic.saturation = saturationCurve.Evaluate(delta);
        modelSettings.basic.contrast = constrastCurve.Evaluate(delta);

        modelSettings.channelMixer.red = new Vector3
            (
                red_R_Curve.Evaluate(delta), 
                0,
                0
            );

        modelSettings.channelMixer.green = new Vector3
            (
                green_R_Curve.Evaluate(delta),
                green_G_Curve.Evaluate(delta),
                green_B_Curve.Evaluate(delta)
             );

        modelSettings.channelMixer.blue = new Vector3
            (
                blue_R_Curve.Evaluate(delta),
                blue_G_Curve.Evaluate(delta),
                blue_B_Curve.Evaluate(delta)
            );

        Profile.colorGrading.settings = modelSettings;
    }

    public void SetupVisuals(ICameraVisualEffectUser visualEffectUser, float effectDuration)
    {
        Profile = visualEffectUser.GetPostProcessClass();

        Duration = effectDuration - 0.1f;

        visualsTask = new ShadowCrwalVisualsTask(this);

        visualsTask.StartTask();
    }
}