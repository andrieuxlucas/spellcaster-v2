﻿using System.Collections;
using ScanningMethods;
using UnityEngine;

namespace Spells.Behaviours
{
    public class DazzlingBoltBehaviour : CustomDirectionalSpell<Visuals.DazzlingBoltVisuals>
    {
        Coroutine hitFailedRoutine;

        private void Start()
        {
            spellVisuals.CreateVisual();

            SetupDirectionalScan(transform.position + transform.forward, transform.forward);
        }

        protected virtual void Update()
        {
            if (!DirectionalScan.ReachedMaxTravelDistance())
            {
                if (DirectionalScan.Scan())
                {
                    OnCollisionRegistered();

                    StartCoroutine(HitRegistrationSuccessfulRoutine());
                }
            }
            else if (hitFailedRoutine == null)
                hitFailedRoutine = StartCoroutine(HitRegistrationFailedRoutine());
        }


        protected virtual void OnCollisionRegistered()
        {
            spellVisuals.TriggerParticleCollision(DirectionalScan.GetHitRegistrationOnCapsuleOverlap(0.1f) /*GetHitRegistrationOnCapsuleOverlap(0.1f)*/);

            foreach (var collider in DirectionalScan.GetOvelappingColliders())
            {
                EffectReceiver receiver = collider.GetComponent<EffectReceiver>();
                if (receiver) ApplyTargetEfects(receiver);
            }
        }

        /// <summary>
        /// The <see cref="Coroutine"/> started when the scanning IS successful.
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerator HitRegistrationSuccessfulRoutine()
        {
            spellVisuals.DiableVisuals();

            yield return new WaitForEndOfFrame();

            Destroy(gameObject);
        }

        /// <summary>
        /// The <see cref="Coroutine"/> started when the scanning is NOT successful.
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerator HitRegistrationFailedRoutine()
        {
            yield return new WaitForEndOfFrame();

            spellVisuals.DiableVisuals();
            Destroy(gameObject, 2.0f);
        }
    }
}