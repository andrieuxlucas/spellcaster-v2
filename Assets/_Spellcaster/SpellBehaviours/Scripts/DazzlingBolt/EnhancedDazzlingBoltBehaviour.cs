﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spells.Behaviours
{
    public class EnhancedDazzlingBoltBehaviour : DazzlingBoltBehaviour
    {
        public bool HasJumpsLeft => jumpCount < maxJumpCount;

        protected Vector3 startingPosition, previousScanPosition, currentScanPosition;

        [Header("Enhanced properties")]
        public float jumpRadius = 5.0f;
        public float jumpInterval = 0.25f;
        public uint maxJumpCount = 2;

        Character currentTargetCharacter;
        bool isDelaying = false;
        short jumpCount = 0;


        protected override IEnumerator HitRegistrationSuccessfulRoutine()
        {
            if (HasJumpsLeft)
            {
                Vector3 nextJumpStartingLocation = GetNextJumpStartLocation(0.1f);

                Character nextCharacter = GetClosestTargetableCharacter(nextJumpStartingLocation);
                if (nextCharacter)
                {
                    currentTargetCharacter = nextCharacter;
                    Vector3 nextJumpTargetLocation = nextCharacter.transform.position;
                    
                    DebugExtension.DebugPoint(nextJumpStartingLocation, Color.yellow, .3f, 3.0f);
                    DebugExtension.DebugPoint(nextJumpTargetLocation, Color.red, .3f, 3.0f);

                    yield return StartNextJumpRoutine(nextJumpStartingLocation, nextJumpTargetLocation);
                }
                else yield return HitRegistrationFailedRoutine();
            }
            else yield return HitRegistrationFailedRoutine();
        }

        protected override void Update()
        {
            //Since the scanning logic is executed by the base clases's update
            // we only want to execute it if this component is not in the
            // "between jump" phase.
            if (!isDelaying)
                base.Update();
        }


        IEnumerator StartNextJumpRoutine(Vector3 nextJumpStartingLocation, Vector3 nextJumpTargetLocation)
        {
            spellVisuals.DiableVisuals();
            isDelaying = true;
            jumpCount++;

            yield return new WaitForSeconds(jumpInterval);

            SetupNextJumpRoutine(nextJumpStartingLocation, nextJumpTargetLocation);
            isDelaying = false;
            spellVisuals.CreateVisual();
        }

        void SetupNextJumpRoutine(Vector3 jumpFrom, Vector3 jumpTo, float startingPositionOffset = 1.0f)
        {
            transform.position = jumpFrom;
            transform.rotation = Quaternion.LookRotation((jumpTo - jumpFrom).normalized, Vector3.up);
            transform.position += transform.forward * startingPositionOffset;

            startingPosition = previousScanPosition = currentScanPosition = transform.position;
        }

        bool CharacterIsVisibleFromLocation(Character character, Vector3 location)
        {
            Vector3 direction = (character.transform.position - location).normalized;
            if (Physics.Raycast(location, direction, out RaycastHit rayHit, Mathf.Infinity, scanableLayers))
            {
                Character characterFound = rayHit.collider.GetComponent<Character>();
                if (characterFound && characterFound.Equals(character))
                    return true;    
            }

            return false;
        }

        Vector3 GetNextJumpStartLocation(float normalOffset = 1.0f)
        {
            if (Physics.Raycast(new Ray(previousScanPosition, (currentScanPosition - previousScanPosition).normalized), out RaycastHit hit, Mathf.Infinity))
            {
                Character characterHitted = hit.collider.GetComponent<Character>();
                if (characterHitted)
                    return hit.point;

                else return hit.point + (hit.normal * normalOffset);
            }

            return (previousScanPosition + currentScanPosition) / 2.0f;
        }

        Character GetClosestTargetableCharacter(Vector3 from)
        {
            Character closestCharacter = null;
            float closestDistance = float.MaxValue;

            Collider[] radius = Physics.OverlapSphere(currentScanPosition, jumpRadius);
            foreach (var collider in radius)
            {
                Character thisCharacter = collider.GetComponent<Character>();
                if (thisCharacter)
                {
                    if (!thisCharacter.Equals(caster) && !thisCharacter.Equals(currentTargetCharacter))
                    {
                        if (CharacterIsVisibleFromLocation(thisCharacter, from))
                        {
                            float thisDistance = Vector3.Distance(from, thisCharacter.transform.position);
                            if (thisDistance < closestDistance)
                            {
                                closestCharacter = thisCharacter;
                                closestDistance = thisDistance;
                            }
                        }
                    }
                }
            }

            return closestCharacter;
        }
    }
}