﻿/**
 *	DazzlingBoltVisuals.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	08/06/2019	(dd/mm/yy)
 */

using UnityEngine;

namespace Spells.Visuals
{
    public class DazzlingBoltVisuals : SpellVisuals
    {
        [SerializeField] GameObject particleVisual;
        RFX4_TransformMotion transformMotion;
        GameObject particleInstance;

        public override void CreateVisual()
        {
            particleInstance = Instantiate(particleVisual, transform.position, transform.rotation);
            transformMotion = particleInstance.GetComponentInChildren<RFX4_TransformMotion>();
            transformMotion.DisableColision();
        }

        public override void DiableVisuals()
        {
            Destroy(particleInstance, 2.0f);
        }

        public override void EnableVisuals()
        {
            particleInstance.gameObject.SetActive(true);
        }

        public void TriggerParticleCollision(RaycastHit hit)
        {
            transformMotion.TriggerParticleCollision(hit);
        }
    }
}