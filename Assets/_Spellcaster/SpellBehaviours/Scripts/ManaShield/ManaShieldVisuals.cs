﻿/**
 *	ManaShieldVisuals.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	10/06/2019	(dd/mm/yy)
 */

using UnityEngine;

namespace Spells.Visuals
{
    public class ManaShieldVisuals : SpellVisuals
    {
        [SerializeField] GameObject particleVisual;
        RFX4_TransformMotion transformMotion;
        GameObject particleInstance;

        public override void CreateVisual()
        {
            particleInstance = Instantiate(particleVisual, transform.position, Quaternion.identity);
            particleInstance.transform.rotation = transform.rotation;
        }

        public override void DiableVisuals()
        {
            Destroy(particleInstance, 5.0f);
        }

        public override void EnableVisuals()
        {
            particleInstance.SetActive(true);
        }

        private void Start()
        {
            CreateVisual();
        }
    }
}