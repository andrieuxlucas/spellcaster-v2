﻿/**
 *	ManaShieldBehaviour.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	10/06/2019	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Spells.Behaviours
{
    using Effects;
    public class ManaShieldBehaviour : GenericSpellBehaviour<Visuals.ManaShieldVisuals>
    {
        [SerializeField] GameObject collisionMesh;
        Rigidbody body;

        [Header("Settings")]
        [SerializeField] protected Vector3 offset;
        [SerializeField] protected float duration = 1.5f;
        [SerializeField] protected float collisionDisableDelay = 1.3f;

        public override void DebugSpell()
        {
            DebugExtension.DebugPoint(transform.position, collisionMesh.activeInHierarchy ? Color.cyan : Color.red, 1f);
        }

        public void SetDuration(float duration)
        {
            this.duration = duration;
        }


        private void Start()
        {
            body = gameObject.AddComponent<Rigidbody>();
            body.isKinematic = true;

            Vector3 baricenter = (caster.Sockets.LeftHandPosition + caster.Sockets.RightHandPosition) / 2.0f;
            transform.position = baricenter + transform.TransformDirection(offset);

            caster.AddRelativeForce((GetCasterEffects()[0] as KnockbackEffectForCharacter).KnockbackForce);

            collisionMesh.SetActive(true);

            StartCoroutine(OnCastRoutine(duration));

            StartCoroutine(CollisionDisableRoutine(collisionDisableDelay));
        }

        IEnumerator OnCastRoutine(float shieldDuration)
        {
            yield return new WaitForSeconds(shieldDuration);

            Destroy(gameObject);
            spellVisuals.DiableVisuals();
        }

        IEnumerator CollisionDisableRoutine(float collisionDisableDelay)
        {
            yield return new WaitForSeconds(collisionDisableDelay);

            collisionMesh.SetActive(false);
        }
    }
}