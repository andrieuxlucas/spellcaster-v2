﻿/**
 *	LifeLinkBehaviour.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	30/06/2019 (dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Spells.Behaviours
{
    public class LifeLinkBehaviour : CustomDirectionalSpell<Visuals.LifeLinkVisuals>
    {
        private void Start()
        {
            SetupDirectionalScan(transform.position + transform.forward, transform.forward);
        }

        private void Update()
        {
            if(!DirectionalScan.ReachedMaxTravelDistance())
            {
                if(DirectionalScan.Scan())
                {
                    OnCollisionProcess();
                }
            }
        }

        void OnCollisionProcess()
        {
            foreach (var collider in DirectionalScan.GetOvelappingColliders())
            {
                if(collider.GetComponent<Character>())
                {
                    spellVisuals.CreateVisual();
                    spellVisuals.SetupVisuals(Caster.transform, collider.transform);
                    return;
                }
            }
        }
    }
}