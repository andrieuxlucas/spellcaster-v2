﻿/**
 *	LifeLinkVisuals.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	02/07/2019 (dd/mm/yy)
 */

using UnityEngine;
namespace Spells.Visuals
{
    public class LifeLinkVisuals : SpellVisuals
    {
        public LifeLinkVisualInstance particleInstancePrefab;
        LifeLinkVisualInstance instantiatedParticleInstance;

        public override void CreateVisual()
        {
            instantiatedParticleInstance = Instantiate(particleInstancePrefab, transform.position, Quaternion.identity);
        }

        public override void DiableVisuals()
        {
            throw new System.NotImplementedException();
        }

        public override void EnableVisuals()
        {
            throw new System.NotImplementedException();
        }

        public void SetupVisuals(Transform caster, Transform target)
        {
            instantiatedParticleInstance.SetFrom(caster);
            instantiatedParticleInstance.SetTo(target);
        }
    }
}