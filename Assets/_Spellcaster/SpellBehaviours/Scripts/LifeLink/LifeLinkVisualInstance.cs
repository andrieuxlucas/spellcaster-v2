﻿/**
 *	LifeLinkVisualInstance.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	30/06/2019 (dd/mm/yy)
 */

using UnityEngine;

namespace Spells.Visuals
{
    public class LifeLinkVisualInstance : MonoBehaviour
    {
        delegate void ParticleAction(ref ParticleSystem.Particle particle);

        Vector3 LinkDirection => (to.position - from.position).normalized;
        Vector3 TransformedOffset => from.TransformDirection(offset);

        [Header("Components")]
        [SerializeField] ParticleSystem system;
        [SerializeField] AnimationCurve curve;

        [Header("References - Targets")]
        [SerializeField] Transform from;
        [SerializeField] Transform to;

        [Header("Options")]
        [SerializeField] bool randomOffset = false;
        [SerializeField] float randomOffsetCeiling = 1.0f;

        [Header("Values - Offset")]
        public Vector3 offset;
        float tick;


        public void Reset()
        {
            system = GetComponent<ParticleSystem>();
            system.Clear();
        }

        private void Awake()
        {
            system = GetComponent<ParticleSystem>();
        }

        private void OnEnable()
        {
            Reset();
        }

        private void Start()
        {
            if (randomOffset)
                RandomizeOffset();
        }

        private void Update()
        {
            if (from && to)
            {
                system.Emit(1);
                ForeachParticle((ref ParticleSystem.Particle Particle) =>
                {
                    Vector3 finalOffset = Vector3.LerpUnclamped(Vector3.zero, TransformedOffset, -1.0f + Mathf.PingPong(tick, 2.0f));
                    Vector3 positionInTime = Vector3.Lerp(from.position, to.position, Particle.remainingLifetime);
                    Vector3 finalPosition = positionInTime + (finalOffset * curve.Evaluate(Particle.remainingLifetime));

                    Particle.position = finalPosition;

                }, GetParticles());

                tick += Time.deltaTime;
            }
        }


        public void SetFrom(Transform transform)
        {
            from.SetParent(transform);
            from.localPosition = Vector3.zero;
        }

        public void SetTo(Transform transform)
        {
            to.SetParent(transform);
            to.localPosition = Vector3.zero;
        }

        [ContextMenu("Randomize offset")]
        public void RandomizeOffset()
        {
            offset = new Vector3()
            {
                x = Random.Range(-randomOffsetCeiling, randomOffsetCeiling),
                y = Random.Range(-randomOffsetCeiling, randomOffsetCeiling),
                z = Random.Range(-randomOffsetCeiling, randomOffsetCeiling)
            };
        }


        void ForeachParticle(ParticleAction particleDelegate, ParticleSystem.Particle[] particles)
        {
            for (int i = 0; i < system.particleCount; i++)
                particleDelegate(ref particles[i]);

            system.SetParticles(particles);
        }

        ParticleSystem.Particle[] GetParticles()
        {
            ParticleSystem.Particle[] particles = new ParticleSystem.Particle[system.particleCount];
            system.GetParticles(particles);

            return particles;
        }
    }
}