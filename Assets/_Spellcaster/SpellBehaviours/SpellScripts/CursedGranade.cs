﻿/**
 *	ArcaneRay.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	27/11/2018	(dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spells
{
    [CreateAssetMenu(menuName = "Spellcaster/Spells/CursedGranade")]
    public class CursedGranade : Spell
    {
        [Header("Cursed granade specific")]
        [SerializeField] protected float _explosionRadius;

        protected override SpellBehaviour Cast(Character caster, Vector3 castFrom, Quaternion castRotation)
        {
            throw new System.NotImplementedException();
        }

        protected override SpellBehaviour GetBehaviour()
        {
            throw new System.NotImplementedException();
        }

        //protected override void OnCollisionHit(GameObject hittedObject, Vector3 collisionPosition)
        //{
        //    var __collisionArray = Physics.OverlapSphere(collisionPosition, _explosionRadius, LayerMask.GetMask(GameDefinitions.PLAYER_OBJECT_LAYER));
        //    var __collisionList = Physics.SphereCastAll(new Ray(collisionPosition, Vector3.up), _explosionRadius, Mathf.Infinity, LayerMask.GetMask(GameDefinitions.PLAYER_OBJECT_LAYER));
        //    foreach (var __item in __collisionArray)
        //    {
        //        EffectReceiver __effectReceiver = __item.transform.GetComponent<EffectReceiver>();
        //        if (__effectReceiver)
        //            foreach (var __effect in _targetEffects)
        //                __effectReceiver.ApplyEffect(__effect);
        //    }
        //}
    }
}