﻿/**
 *	LifeLink.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	02/07/2019 (dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Spells
{
    [CreateAssetMenu(menuName = "Spellcaster/Spells/Life link")]
    public class LifeLink : CustomSpell<Behaviours.LifeLinkBehaviour>
    {

    }
}