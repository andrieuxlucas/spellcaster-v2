﻿/**
 *	ManaBlast.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	01/12/2018	(dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spells
{
    [CreateAssetMenu(menuName = (GameDefinitions.SCRIPTABLE_SPELLS + "Mana Blast"))]
    public class ManaBlast : Spell
    {
        protected override SpellBehaviour Cast(Character caster, Vector3 castFrom, Quaternion castRotation)
        {
            throw new System.NotImplementedException();
        }

        protected override SpellBehaviour GetBehaviour()
        {
            throw new System.NotImplementedException();
        }
    }
}