﻿/**
 *	DazzlingBolt.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spells
{
    [CreateAssetMenu(menuName = "Spellcaster/Spells/Dazzling bolt")]
    public class DazzlingBolt : CustomSpell<Behaviours.DazzlingBoltBehaviour>
    {

    }
}