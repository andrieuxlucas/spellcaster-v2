﻿/**
 *	ShadowCrawl.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	16/06/2019 (dd/mm/yy)
 */

using UnityEngine;

namespace Spells
{
    using Behaviours;
    [CreateAssetMenu(menuName = (GameDefinitions.SCRIPTABLE_SPELLS + "Shadow Crawl"))]
    public class ShadowCrawl : CustomSpell<ShadowCrawlBehaviour>
    {
        [Header("Custom values")]
        [SerializeField] protected float duration = 2.0f;

        protected override SpellBehaviour Cast(Character caster, Vector3 castFrom, Quaternion castRotation)
        {
            ShadowCrawlBehaviour instance = (ShadowCrawlBehaviour)base.Cast(caster, castFrom, castRotation);
            instance.SetDuration(duration);

            return instance;
        }
    }
}