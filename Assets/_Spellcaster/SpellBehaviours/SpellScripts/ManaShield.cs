﻿/**
 *	ManaShield.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	10/06/2019	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Spells
{
    using Behaviours;

    [CreateAssetMenu(menuName = (GameDefinitions.SCRIPTABLE_SPELLS + "Mana Shield"))]
    public class ManaShield : CustomSpell<ManaShieldBehaviour>
    {
        [Header("Custom values")]
        [SerializeField] protected float duration;

        protected override SpellBehaviour Cast(Character caster, Vector3 castFrom, Quaternion castRotation)
        {
            ManaShieldBehaviour instance = (ManaShieldBehaviour)base.Cast(caster, castFrom, castRotation);
            instance.SetDuration(duration);

            return instance;
        }
    }
}