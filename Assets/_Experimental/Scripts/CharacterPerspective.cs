﻿/**
 *	CharacterPerspective.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	28/11/2018	(dd/mm/yy)
 */

using UnityEngine;
using UnityEngine.PostProcessing;

public abstract class CharacterPerspective : MonoBehaviour, ICameraVisualEffectUser
{
    public abstract Quaternion Rotation { get; }

    public abstract PostProcessingProfile GetPostProcessClass();

    public abstract void TurnView(float horizontalTurn, float verticalTurn);

    public abstract Quaternion GetRotationFromPositionToCameraTargetPoint(Vector3 from);
}