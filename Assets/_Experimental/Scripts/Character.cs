﻿/**
 *	Charater.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using UnityEngine;

public abstract class Character : MonoBehaviour, IHeightAdjustable, ISpeedModulatable
{
    public abstract bool CanCrouch { get; }
    public abstract bool CanMove { get; }
    public abstract bool CanTurn { get; }
    public abstract bool CanJump { get; }
    public abstract bool CanRun { get; }

    public CharacterSockets Sockets { get { return _playerSockets; } }
    public CharacterAttributes Attributes { get { return _attributes; } }
    public CharacterPerspective Perspective { get { return _perspective; } }
    public CharacterMovementComponent MovementComponent { get { return _movementComponent; } }

    [Header("Components")]
    [SerializeField] protected CharacterMovementComponent _movementComponent;
    [SerializeField] protected CharacterEffectReceiver _effectReceiver;
    [SerializeField] protected CharacterPerspective _perspective;
    [SerializeField] protected CharacterAttributes _attributes;
    [SerializeField] protected CharacterSockets _playerSockets;

    private void Awake()
    {
        if (!_movementComponent) _movementComponent = GetComponent<CharacterMovementComponent>();
        if (!_effectReceiver) _effectReceiver = GetComponent<CharacterEffectReceiver>();
        if (!_perspective) _perspective = GetComponent<CharacterPerspective>();
        if (!_attributes) _attributes = GetComponent<CharacterAttributes>();
        if (!_playerSockets) _playerSockets = GetComponent<CharacterSockets>();
    }

    public virtual void AddRelativeForce(Vector3 vector)
    {
        _movementComponent.AddRelativeForce(vector);
    }

    public virtual void AddForce(Vector3 vector)
    {
        _movementComponent.AddForce(vector);
    }


    public virtual void TurnPerspective(float horizontalTurn, float verticalTurn)
    {
        _perspective.TurnView(horizontalTurn, verticalTurn);
    }

    public virtual void Move(float sidewaysMoviment, float forwardMoviment)
    {
        _movementComponent.Move(sidewaysMoviment, forwardMoviment);
    }

    public virtual void Run(float sidewaysMoviment, float forwardMoviment)
    {
        _movementComponent.Run(sidewaysMoviment, forwardMoviment);
    }

    public virtual void PowerSlide()
    {
        _movementComponent.PowerSlide();
    }

    public virtual void StopRun()
    {
        _movementComponent.StopRun();
    }

    public virtual void Crouch()
    {
        if (_movementComponent.IsCrouched) _movementComponent.Stand();

        else _movementComponent.Crouch();
    }

    public virtual void Jump()
    {
        _movementComponent.Jump();
    }


    public abstract void CastSecondarySpell();

    public abstract void CastPrimarySpell();


    public void ApplyEffect(Effect effect)
    {
        _effectReceiver.ApplyEffect(effect);
    }


    public abstract void SetSpeedPercentage(float percentage);
    public abstract void SetHeight(float newHeight);
}