﻿/**
 *	PlayerCharacter.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using UnityEngine;

public class PlayerCharacter : Character
{
    public ComboReaderComponent CombroReaderComponent { get { return _comboReader; } }

    public override bool CanCrouch => true;
    public override bool CanMove => true;
    public override bool CanTurn => true;
    public override bool CanJump => true;
    public override bool CanRun => true;

    [SerializeField] private ComboReaderComponent _comboReader;
    [SerializeField] private ArcaneArmory _arcaneArmory;

    private void OnEnable()
    {
        if(_movementComponent)
        {
            _movementComponent.FinishFall += OnFallFinished;
        }

        if (_comboReader && _arcaneArmory) _comboReader.ComboCompleted += _arcaneArmory.Conjure;
    }

    private void OnDisable()
    {
        if (_movementComponent)
        {
            _movementComponent.FinishFall -= OnFallFinished;
        }

        if (_comboReader && _arcaneArmory) _comboReader.ComboCompleted -= _arcaneArmory.Conjure;
    }


    public void StartBlink()
    {
    }

    public void FinishBlink()
    {
    }

    public void InterruptBlink()
    {
    }


    public override void CastSecondarySpell()
    {
        if (_arcaneArmory)
            _arcaneArmory.CastSecondarySpell(this, _playerSockets.RightHandPosition, _perspective.GetRotationFromPositionToCameraTargetPoint(_playerSockets.RightHandPosition)/*_perspective.Rotation*/);
    }

    public override void CastPrimarySpell()
    {
        if (_arcaneArmory)
            _arcaneArmory.CastPrimarySpell(this, _playerSockets.LeftHandPosition, _perspective.GetRotationFromPositionToCameraTargetPoint(_playerSockets.LeftHandPosition)/*_perspective.Rotation*/);
    }


    private void OnFallFinished(FallInfo fallInformation)
    {
    }


    public override void SetSpeedPercentage(float percentage)
    {
        _movementComponent.UseableSpeedPercentage = percentage;
    }

    public override void SetHeight(float newHeight)
    {
        GetComponent<CharacterController>().height = newHeight;
    }
}