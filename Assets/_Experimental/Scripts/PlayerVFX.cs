﻿/**
 *	BlinkVFX.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	13/08/2018	(dd/mm/yy)
 */

using UnityEngine;
using EZCameraShake;
using System.Collections;

public class PlayerVFX : MonoBehaviour
{
    [Header("Particle")]
    [SerializeField] private ParticleSystem _dashParticles;

    public float magnitude = 4f;
    public float roughness = 4f;
    public float fadeIn = .1f;
    public float fadeOut = 1.0f;

    private void Start()
    {
        _dashParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
    }

    public void PlayeBlinkParticles()
    {
        _dashParticles.Play(true);
    }

    public void StopBlinkParticles()
    {
        _dashParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
    }
}