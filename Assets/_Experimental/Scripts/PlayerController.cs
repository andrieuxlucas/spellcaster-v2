﻿/**
 *	PlayerController.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using UnityEngine;

public class PlayerController : Controller<PlayerCharacter>
{
    public override float GetCamHorizontalInput()
    {
        var __mouseInput = Input.GetAxis("Mouse X");

        return __mouseInput != 0 ? __mouseInput : Input.GetAxis("RHorizontal");
    }

    public override float GetCamVerticalInput()
    {
        var __mouseInput = Input.GetAxis("Mouse Y");

        return __mouseInput != 0 ? __mouseInput : Input.GetAxis("RVertical");
    }

    public override float GetHorizontalInput()
    {
        return Input.GetAxis("Horizontal");
    }

    public override float GetVerticalInput()
    {
        return Input.GetAxis("Vertical");
    }

    public override bool GetActionInput()
    {
        throw new System.NotImplementedException();
    }

    public override bool GetJumpInput()
    {
        return Input.GetButton("Jump");
    }


    public override bool GetRunInput() { return Input.GetButton("Run") ? Input.GetButton("Run") : Input.GetAxis("Run") != 0; }

    public override bool GetPrimarySpellInput() { return Input.GetButtonDown("CastPrimary"); }

    public override bool GetSecondarySpellInput() { return Input.GetButtonDown("CastSecondary"); }

    public override bool GetCrouchInput() { return Input.GetButtonDown("Crouch"); }



    public virtual bool GetBlinkInterruptInput()
    {
        return false;
    }

    /// <summary>
    /// Retrieves the state of the key for finishing the blink.
    /// </summary>
    /// <returns>Is the key pressed?</returns>
    public virtual bool GetBlinkReleaseInput()
    {
        return Input.GetMouseButtonUp(1);
    }

    /// <summary>
    /// Retrieves the state of the key for triggering the blink.
    /// </summary>
    /// <returns>Is the key pressed?</returns>
    public virtual bool GetBlinkPressedInput()
    {
        return Input.GetMouseButtonDown(1);
    }

    /// <summary>
    /// Called wach frame by its parent class.
    /// </summary>
    protected override void OnUpdate()
    {
        base.OnUpdate();

        //if (_charater.CanMove)
        //{
        //    PlayerPrespective __perspective = (PlayerPrespective)_charater.Perspective;
        //    if (__perspective)
        //    {
        //        __perspective.SetCameraTilt(-GetAnalogHorizontalInput());
        //    }
        //}

        // If we're pressed the blink button
        if (GetBlinkPressedInput())
            _charater.StartBlink();

        // If we released the blink button
        else if (GetBlinkReleaseInput())
            _charater.FinishBlink();

        // If we pressed the interaction interrupt button
        else if (GetBlinkInterruptInput())
            _charater.InterruptBlink();
    }
}