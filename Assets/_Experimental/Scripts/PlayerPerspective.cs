﻿/**
 *	PlayerPrespective.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using EZCameraShake;

using UnityEngine;
using UnityEngine.PostProcessing;

public class PlayerPerspective : CharacterPerspective
{
    public override Quaternion Rotation { get { return _playerCamera.transform.rotation; } }

    public LayerMask ExcludePlayerMask { get { return _excludePlayerMask; } }
    public Camera PlayerCamera { get { return _playerCamera; } }
    public Camera WorldCamera { get { return _worldCamera; } }

    public Vector3 CameraForward
    {
        get
        {
            if (_playerCamera)
                return _playerCamera.transform.forward;

            else
                return Vector3.zero;
        }
    }
    public Vector3 MousePosition
    {
        get
        {
            if (_playerCamera)
                return _playerCamera.ViewportToWorldPoint(Input.mousePosition);

            else
                return Vector3.zero;
        }
    }
    public Vector3 CameraPosition
    {
        get
        {
            if (_playerCamera)
                return _playerCamera.transform.position;

            else
                return Vector3.zero;
        }
    }
    public Vector3 DirectionToMouse
    {
        get
        {
            if (_playerCamera)
                return (MousePosition - _playerCamera.transform.position).normalized;

            else
                return Vector3.zero;
            
        }
    }
    public Ray RayFromCameraToMouse
    {
        get
        {
            if (_playerCamera)
                return _playerCamera.ScreenPointToRay(Input.mousePosition);

            else
                return new Ray();
        }
    }

    [Header("Layers")]
    [SerializeField] private LayerMask _excludePlayerMask;

    [Header("Components")]
    [SerializeField] private Transform _playerBody;
    [SerializeField] private Camera _playerCamera;
    [SerializeField] private Camera _worldCamera;

    [Header("Camera components")]
    [SerializeField] private CameraShaker _cameraShaker;

    [Header("Settings")]
    [SerializeField, Range(0, 90)] float _maxUpAngle = 80.0f;
    [SerializeField, Range(0, 90)] float _maxDownAngle = 80.0f;
    [SerializeField] private float _horizontalTurnRate = 120.0f;
    [SerializeField] private float _verticalTurnRate = 60.0f;

    [Space(10)]
    [SerializeField] private CameraShakerSettings _shakeSettings;

    private void Awake()
    {
        if (!_playerBody) _playerBody = GetComponent<Transform>();
        if (!_playerCamera) _playerCamera = GetComponent<Camera>();
    }

    public override Quaternion GetRotationFromPositionToCameraTargetPoint(Vector3 from)
    {
        Vector3 projectionPoint = Vector3.zero;

        if (Physics.Raycast(RayFromCameraToMouse, out RaycastHit hit, Mathf.Infinity, ExcludePlayerMask))
            projectionPoint = hit.point;
        else projectionPoint = RayFromCameraToMouse.GetPoint(40f);

        DebugExtension.DebugPoint(projectionPoint, .7f, 3f);
        return Quaternion.LookRotation((projectionPoint - from).normalized, Vector3.up);
    }

    public override void TurnView(float horizontalTurn, float verticalTurn)
    {
        if (_playerBody)
            _playerBody.Rotate(Vector3.up * horizontalTurn * _horizontalTurnRate * Time.deltaTime);

        if(_playerCamera)
            _playerCamera.transform.Rotate(Vector3.right * verticalTurn * _verticalTurnRate * Time.deltaTime);

        float __dotForwarAndUp = Vector3.Dot(CameraForward, Vector3.up);
        float __angle = Mathf.Acos(__dotForwarAndUp) * Mathf.Rad2Deg;
        
        // Looking up
        if(verticalTurn < 0)
        {
            // Went through the upper threshold
            if (__angle < (90 - _maxUpAngle))
                _playerCamera.transform.localRotation = Quaternion.Euler(new Vector3(-_maxUpAngle, 0, 0));
        }

        // Looking down
        else if(verticalTurn > 0)
        {
            // Went through the botom threshold
            if (__angle > (90 + _maxDownAngle))
                _playerCamera.transform.localRotation = Quaternion.Euler(new Vector3(_maxDownAngle, 0, 0));
        }
    }

    public override PostProcessingProfile GetPostProcessClass()
    {
        return _playerCamera.GetComponent<PostProcessingBehaviour>().profile;
    }
}