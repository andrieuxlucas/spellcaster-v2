﻿/**
 *	AttackState.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	14/08/2018	(dd/mm/yy)
 */

public enum AttackState
{
	Idle, Stabbing, Punching, Attacking
}