﻿/**
 *	CrouchState.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	16/08/2018	(dd/mm/yy)
 */

public enum CrouchState
{
    Standing, Crouching, Sliding
}