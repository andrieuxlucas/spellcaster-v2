﻿/**
 *	LoopableEffect.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on: 18/05/2018	(dd/mm/yy)
 */

using System.Collections;
using UnityEngine;

namespace ProgrammingResources.Effect
{
    /// <summary>
    /// Base class for effects containing a Loop(update cycle). Implements <see cref="ILoopableEffect"/>
    /// </summary>
    public abstract class LoopableEffect : MonoBehaviour, ILoopableEffect
    {
        [Header("Loopable effect")]
        [Tooltip("If the effect should be started automatically on the begin of the scene.")]
        [SerializeField] protected bool startActive = false;
        [Tooltip("If the effect, when trigerred, should run indefinitely.")]
        [SerializeField] protected bool isInfinite = false;
        [SerializeField] [HideInInspector] protected bool isActive = false;

        /// <summary>
        /// Variable that controlls if the StopEffect() logic should be called at the end of the effect loop.
        /// </summary>
        [Tooltip("If the method StopEffect() should be called at the end of the effect loop.")]
        [SerializeField] protected bool stopAtTheEnd = true;
        [Tooltip("The duration of the effect.")]
        [SerializeField] protected float duration = 1.0f;
        [Tooltip("The 'step' (or inteval) between the update cycles of the effect. Lesser numbers means " +
            "smoother effects at the cost of being slightly more performance heavy.")]
        [SerializeField] protected float interval = 0.2f;
        protected float elapsedTime = 0.0f;

        [SerializeField] [HideInInspector] protected System.Action<float> effectDelegate;
        [SerializeField] [HideInInspector] protected Coroutine effectLoopRoutine;

        /// <summary>
        /// Is this effect currently active ?
        /// </summary>
        public bool IsActive { get { return isActive; } }
        /// <summary>
        /// Should this effect run indefinitely ?
        /// </summary>
        public bool IsInfinite { get { return isInfinite; } set { isInfinite = value; elapsedTime = 0.0f; } }
        /// <summary>
        /// Should this effect begins its execution with the scene ?
        /// </summary>
        public bool StartActive { get { return startActive; } set { startActive = value; } }

        /// <summary>
        /// The duration of this effect.
        /// </summary>
        public float Duration { get { return duration; } set { duration = value; } }
        /// <summary>
        /// The interval between this effect update's logic.
        /// </summary>
        public float Interval { get { return interval; } set { interval = value; } }
        /// <summary>
        /// The time elapsed since the effect started.
        /// </summary>
        public float ElapsedTime { get { return elapsedTime; } }
        /// <summary>
        /// The ratio [0 - 1] using <see cref="ElapsedTime"/> divided by the <see cref="Duration"/> of the effect.
        /// (0 if <see cref="IsInfinite"/> == true).
        /// </summary>
        public float Delta
        {
            get
            {
                if (isInfinite)
                    return elapsedTime;

                else if (!isInfinite && duration <= 0)
                    return 0;

                else
                    return Mathf.Clamp01(elapsedTime / duration);
            }
        }

        /// <summary>
        /// The transform of this effect object.
        /// </summary>
        public virtual Transform Transform { get { return transform; } }

        /// <summary>
        /// The update method of the effect.
        /// </summary>
        /// <returns>The loop coroutine of this effect.</returns>
        public virtual IEnumerator EffectLoop()
        {
            // If its an infinite effect, loops while the it is set as isInfinite.
            // Else if is not infinite, loops while elapsedTime is less than duration
            while(isInfinite ? isInfinite : elapsedTime < duration)
            {
                // Assuming that we have an effect method, we invoke it
                if (effectDelegate != null)
                {
                    effectDelegate(Delta);
                }

                // If the interval is greater than 0 (there is intervall between effect logic)
                if (interval > 0)
                {
                    // Increments the elapsedTime by the amount of (interval), and yields the same value
                    elapsedTime += interval * Time.timeScale;
                    yield return new WaitForSeconds(interval);
                }

                // If there is no interval between effect logic
                else
                {
                    // Increments the elapsedTime by Time.deltaTime, and yields the same value
                    elapsedTime += Time.deltaTime;
                    yield return null;
                }
            }

            // Applies the effect one last time (out of the loop assuming Delta = 1).
            effectDelegate(1);

            if (stopAtTheEnd) StopEffect();
        }

        /// <summary>
        /// Stop this target effect.
        /// </summary>
        public virtual void StopEffect()
        {
            if (effectLoopRoutine != null)
            {
                StopCoroutine(effectLoopRoutine);

                effectLoopRoutine = null;

                elapsedTime = 0.0f;

                isActive = false;
            }
        }

        /// <summary>
        /// Starts this target effect.
        /// </summary>
        public virtual void TriggerEffect()
        {
            isActive = true;
            elapsedTime = 0;
            effectLoopRoutine = StartCoroutine(EffectLoop());
        }

        public abstract void Effect(float delta);

        protected virtual void Awake()
        {
            effectDelegate = Effect;
        }
        protected virtual void Start()
        {
            if (startActive) TriggerEffect();
        }
    }
}