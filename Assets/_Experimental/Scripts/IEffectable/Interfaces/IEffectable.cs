﻿/**
 *	IEffectable.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on: 18/05/2018	(dd/mm/yy)
 */

using UnityEngine;

namespace ProgrammingResources.Effect
{
    /// <summary>
    /// Interface which contains the base implementation methods and properties for an effect.
    /// </summary>
    public interface IEffectable
    {
        /// <summary>
        /// The transform of the effect object.
        /// </summary>
        Transform Transform { get; }
        /// <summary>
        /// Is this effect currently active ?
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Main method for triggering the target effect.
        /// </summary>
        void TriggerEffect();
        /// <summary>
        /// Main method for stopping the target effect
        /// </summary>
        void StopEffect();
    }
}