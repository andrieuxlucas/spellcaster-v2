﻿/**
 *	ILoopableEffect.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on: 18/05/2018	(dd/mm/yy)
 */

using System.Collections;

namespace ProgrammingResources.Effect
{
    /// <summary>
    /// Base interface containing the implementation of aa loopable effect (effects with an update cycle).
    /// </summary>
    public interface ILoopableEffect : IEffectable
    {
        /// <summary>
        /// The update method of the effect.
        /// </summary>
        /// <returns></returns>
        IEnumerator EffectLoop();
    }
}