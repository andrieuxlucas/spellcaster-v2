﻿/**
 *	ForwardPositiveRunBehaviour.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	06/03/2019 (dd/mm/yy)
 */

public class ForwardPositiveRunBehaviour : IMovementRunBehaviour
{
    public bool Run(float sidewaysMovement, float forwardMovement)
    {
        return forwardMovement >= 0;
    }
}