﻿/**
 *	HeightControl.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	13/08/2018	(dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightControl
{
	public Transform Character { get; private set; }
    public ushort Precision { get; private set; }

    private List<float> _previousHeights;
    private float _currentHeight;

    public HeightControl(Transform character, ushort precision)
    {
        Character = character;
        Precision = precision;

        _currentHeight = float.NegativeInfinity;
        _previousHeights = new List<float>(Precision + 1);
    }

    public void UpdateHeight()
    {
        if (_currentHeight.Equals(float.NegativeInfinity))
            _currentHeight = Character.position.y;
        else
        {
            _previousHeights.Add(_currentHeight);

            _currentHeight = Character.position.y;

            if (_previousHeights.Count > Precision)
                _previousHeights.RemoveAt(0);
        }
    }

    public bool IsClambing()
    {
        if (_previousHeights.Count < Precision)
            return false;

        foreach (var __passedHeight in _previousHeights)
            if (__passedHeight > _currentHeight)
                return false;

        return true;
    }

    public bool IsFalling()
    {
        if (_previousHeights.Count < Precision)
            return false;

        foreach (var __passedHeight in _previousHeights)
            if (__passedHeight < _currentHeight)
                return false;

        return true;
    }
}