﻿/**
 *	Controller.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using UnityEngine;

public abstract class Controller<CharacterType> : MonoBehaviour where CharacterType : Character
{
    public CharacterType Character { get { return _charater; } }

    [Header("References")]
    [SerializeField] protected CharacterType _charater;

    private void Awake()
    {
        if (!_charater) _charater = GetComponent<CharacterType>();
    }
    private void Update()
    {
        if(_charater)
        {
            OnUpdate();
        }
    }

    public abstract float GetHorizontalInput();
    public abstract float GetVerticalInput();

    public abstract float GetCamHorizontalInput();
    public abstract float GetCamVerticalInput();

    public abstract bool GetSecondarySpellInput();
    public abstract bool GetPrimarySpellInput();
    public abstract bool GetActionInput();
    public abstract bool GetCrouchInput();
    public abstract bool GetJumpInput();
    public abstract bool GetRunInput();

    protected virtual void OnUpdate()
    {
        // Movementation
        if (_charater.CanMove)
            _charater.Move(GetHorizontalInput(), GetVerticalInput());

        // Perspective turning
        if (_charater.CanTurn)
            _charater.TurnPerspective(GetCamHorizontalInput(), GetCamVerticalInput());

        // Running
        if (_charater.CanRun)
        {
            if (GetRunInput())
                _charater.Run(GetHorizontalInput(), GetVerticalInput());

            else _charater.StopRun();
        }

        // Vertical movement
        if (GetJumpInput())
            _charater.Jump();

        // Crounching
        if (GetCrouchInput())
            if (_charater.CanCrouch)
                _charater.Crouch();

        // Spell casting
        if (GetPrimarySpellInput())
            _charater.CastPrimarySpell();

        if (GetSecondarySpellInput())
            _charater.CastSecondarySpell();
    }
}