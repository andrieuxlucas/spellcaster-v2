﻿/**
 *	CharacterSockets.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using UnityEngine;

public class CharacterSockets : MonoBehaviour
{
    [Header("Animation sockets")]
    [SerializeField] private Transform rightHandSocket;
    [SerializeField] private Transform leftHandSocket;

    public Vector3 RightHandPosition { get { return rightHandSocket.position; } }
    public Vector3 LeftHandPosition { get { return leftHandSocket.position; } }

    public Transform RightHandSocket { get => rightHandSocket; set => rightHandSocket = value; }
    public Transform LeftHandSocket { get => leftHandSocket; set => leftHandSocket = value; }
}